﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pyramid
{
    public class CalculateService
    {
        public static void ComputeSums(List<Participant> pyramid, List<Transfer> transfers)
        {
            if (!pyramid.Any() || !transfers.Any())
            {
                Console.WriteLine("Brak danych w pliku");
                Console.ReadKey();
                Environment.Exit(0);
            }

            List<uint> provisionSequence = new List<uint>();  

            for (int i = 0; i < transfers.Count; i++)
            {
                var amount = transfers[i].Amount;
                provisionSequence.Clear();

                var currentParticipant = FindParticipantById(pyramid[0], transfers[i].FromId);                            
                var superiorId = currentParticipant.DirectSuperiorId;

                if (superiorId == 0)
                {
                    currentParticipant.Sum += transfers[i].Amount;
                }
                else
                {
                    SetProvisionSequence(provisionSequence, superiorId, pyramid[0]);
                }
               
                provisionSequence.Reverse();
          
                AssignProvisionsToParticipants(provisionSequence, pyramid[0], currentParticipant, amount);

            }
        }

        public static void SetProvisionSequence(List<uint> provisionSequence, uint superiorId, Participant founder)
        {
       
            while (superiorId != 0)
            {
                provisionSequence.Add(superiorId);

                var superior = FindParticipantById(founder, superiorId);
                superiorId = superior.DirectSuperiorId;
            }

        }


        public static void AssignProvisionsToParticipants(List<uint> provisionSequence, Participant founder, Participant currentParticipant, uint amount)
        {

            while (provisionSequence.Any()) 
            {
                var currentProvisionerId = provisionSequence.First();
                provisionSequence.RemoveRange(0, 1);

                var currentProvisioner = FindParticipantById(founder, currentProvisionerId);

                if (currentProvisioner.DirectSubordinates.Any(s => s.Id == currentParticipant.Id))
                {
                    currentProvisioner.Sum += amount;
                }
                else
                {
                    currentProvisioner.Sum += amount/2;
                    amount = (amount - amount/2);
                }
            }

        }


        public static Participant FindParticipantById(Participant participant, uint id)
        {

            if (participant.Id == id)
                return participant;

            foreach (Participant part in participant.DirectSubordinates)
            {
                Participant foundParticipant = FindParticipantById(part, id);
                if (foundParticipant != null)
                {
                    return foundParticipant;
                }

            }
     
            return null;
        }

        public static void AddSubordinatesCountsToEachParticipant(Participant participant)
        {

            var amountOfSubordinates = AddSubordinatesCountToParticipant(participant, 0);
            participant.AmountOfSubordinates = amountOfSubordinates;

            foreach (Participant child in participant.DirectSubordinates)
            {
                amountOfSubordinates = AddSubordinatesCountToParticipant(child, 0);
                child.AmountOfSubordinates = amountOfSubordinates;
            }
        }

        public static uint AddSubordinatesCountToParticipant(Participant participant, uint toAdd)
        {

            var a = (uint)participant.DirectSubordinates.Count(p => p.DirectSubordinates.Count == 0);
            toAdd += a;

            return participant.DirectSubordinates.Where(p => p.DirectSubordinates.Count != 0)
                .Aggregate(toAdd, (current, part) => AddSubordinatesCountToParticipant(part, current));
        }

    }
}
