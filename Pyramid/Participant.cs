﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pyramid
{
    public class Participant
    {
        public uint Id { get; set; }
        public ulong Sum { get; set; }
        public uint Level { get; set; }
        public List<Participant> DirectSubordinates { get; set; }
        public uint DirectSuperiorId { get; set; }
        public uint AmountOfSubordinates { get; set; }

    }
}
