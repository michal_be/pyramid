﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pyramid
{
    class ConsoleInteraction
    {
        public static void Print(Participant participant)
        {
            StringBuilder resultToDisplay = new StringBuilder();

            resultToDisplay
                .Append(participant.Id)
                .Append(" ")
                .Append(participant.Level)
                .Append(" ")
                .Append(participant.AmountOfSubordinates)
                .Append(" ")
                .Append(participant.Sum);
         
            Console.WriteLine(resultToDisplay);

            foreach (Participant part in participant.DirectSubordinates)
            {
                Print(part);
            }
        }
    }
}
