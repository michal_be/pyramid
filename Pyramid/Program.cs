﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Pyramid
{
    class Program
    {
        static void Main(string[] args)
        {
         
            List<Participant> pyramid = XmlReaderService.ReadPyramide();
            List<Transfer> transfers = XmlReaderService.ReadTransfers();

            CalculateService.ComputeSums(pyramid, transfers);      
            CalculateService.AddSubordinatesCountsToEachParticipant(pyramid[0]);

            ConsoleInteraction.Print(pyramid[0]);
            Console.ReadKey();
        }

    }
}
