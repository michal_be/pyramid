﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pyramid
{
    class FileVocabularyCase
    {
        public const string TransferCase = "przelew";
        public const string FromCase = "od";
        public const string AmountCase = "kwota";
        public const string PyramidCase = "piramida";
        public const string ParticipantCase = "uczestnik";
        public const string IdCase = "id";
    }
}
