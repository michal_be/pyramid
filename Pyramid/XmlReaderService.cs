﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Pyramid
{
    class XmlReaderService
    {
        private const string TransferFilePath = @"przelewy.xml";
        private const string PyramidFilePath = @"piramida.xml";

        public static List<Transfer> ReadTransfers()
        {
            List<Transfer> transfers = null;

            try
            {
                XDocument xmlDoc = XDocument.Load(TransferFilePath);
                transfers = xmlDoc
                    .Descendants(FileVocabularyCase.TransferCase)
                    .Select(x => new Transfer()
                    {
                        FromId = (uint)x.Attribute(FileVocabularyCase.FromCase),
                        Amount = (uint)x.Attribute(FileVocabularyCase.AmountCase)
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                Environment.Exit(0);
            }

            return transfers;
        }

        public static List<Participant> ReadPyramide()
        {
            List<Participant> pyramid = null;

            try
            {
                XDocument xmlDoc = XDocument.Load(PyramidFilePath);
                uint levelCounter = 0;
                uint directSuperior = 0;
                pyramid = LoadPyramid(xmlDoc.Descendants(FileVocabularyCase.PyramidCase)
                    .Elements(FileVocabularyCase.ParticipantCase), levelCounter, directSuperior);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                Environment.Exit(0);
            }
        
            return pyramid;
        }

        public static List<Participant> LoadPyramid(IEnumerable<XElement> pyramid, uint levelCounter, uint directSuperior)
        {
            uint currentLevel = levelCounter++;

            return pyramid.Select(x => new Participant()
            {
                DirectSuperiorId = directSuperior,
                Id = (uint)x.Attribute(FileVocabularyCase.IdCase),
                Level = currentLevel,
                DirectSubordinates = LoadPyramid(x.Elements(FileVocabularyCase.ParticipantCase), levelCounter, (uint)x.Attribute(FileVocabularyCase.IdCase))
            }).ToList();
        }

    }
}
