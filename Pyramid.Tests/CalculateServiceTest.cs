﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pyramid;

namespace Pyramid.Tests
{
    [TestClass]
    public class CalculateServiceTest
    {
   
        [TestMethod]
        public void WhenComputeSumCalledWithGivenDataThenShouldReturnSumEqualsTo100()
        {

            //Arrange
            var pyramid = new List<Participant>()
            {
                new Participant()
                {
                    AmountOfSubordinates = 0,
                    DirectSubordinates = new List<Participant>(),
                    DirectSuperiorId = 0,
                    Id = 1,
                    Level = 0,
                    Sum = 0
                }
            };

            var transfers = new List<Transfer>()
            {
                new Transfer()
                {
                    Amount = 100,
                    FromId = 1
                }
            };

            ulong expectedSum = 100;

            //Act
            CalculateService.ComputeSums(pyramid, transfers);

            //Assert
            Assert.AreEqual(expectedSum, pyramid.First().Sum);
        }


        [TestMethod]
        public void WhenComputeSumCalledWithGivenDataThenShouldReturnExpectedValues()
        {
            //Arrange
            var pyramid = new List<Participant>()
            {
                new Participant()
                {
                    AmountOfSubordinates = 2,
                    DirectSubordinates = new List<Participant>()
                    {
                        new Participant()
                        {
                            AmountOfSubordinates = 0,
                            DirectSubordinates = new List<Participant>(),
                            DirectSuperiorId = 1,
                            Id = 2,
                            Level = 1,
                            Sum = 0
                        },
                        new Participant()
                        {
                            AmountOfSubordinates = 1,
                            DirectSubordinates = new List<Participant>()
                            {
                                new Participant()
                                {
                                    AmountOfSubordinates = 0,
                                    DirectSubordinates = new List<Participant>(),
                                    DirectSuperiorId = 3,
                                    Id = 4,
                                    Level = 2,
                                    Sum = 0
                                }
                            },
                            DirectSuperiorId = 1,
                            Id = 3,
                            Level = 1,
                            Sum = 0
                        }
                    },
                    DirectSuperiorId = 0,
                    Id = 1,
                    Level = 0,
                    Sum = 0
                }
            };

            var transfers = new List<Transfer>()
            {
                new Transfer()
                {
                    Amount = 100,
                    FromId = 2
                },
                new Transfer()
                {
                    Amount = 50,
                    FromId = 3
                },
                new Transfer()
                {
                    Amount = 100,
                    FromId = 4
                },
                new Transfer()
                {
                    Amount = 200,
                    FromId = 4
                },
                new Transfer()
                {
                    Amount = 0,
                    FromId = 3
                }
            };

            ulong expectedSumForIdEquals1 = 300;
            ulong expectedSumForIdEquals2 = 0;
            ulong expectedSumForIdEquals3 = 150;
            ulong expectedSumForIdEquals4 = 0;

            //Act
            CalculateService.ComputeSums(pyramid, transfers);

            //Assert
            Assert.AreEqual(expectedSumForIdEquals1, pyramid.First().Sum);
            Assert.AreEqual(expectedSumForIdEquals2, pyramid.First().DirectSubordinates.First(s => s.Id == 2).Sum);
            Assert.AreEqual(expectedSumForIdEquals3, pyramid.First().DirectSubordinates.First(s => s.Id == 3).Sum);
            Assert.AreEqual(expectedSumForIdEquals4,
                pyramid.First().DirectSubordinates.First(s => s.Id == 3).DirectSubordinates.First(s => s.Id == 4).Sum);
        }


        [TestMethod]
        public void WhenFindParticipantByIdCalledWithGivenIdThenShouldReturnFoundParticipantObject()
        {
            //Arrange
            var pyramid = new List<Participant>()
            {
                new Participant()
                {
                    AmountOfSubordinates = 2,
                    DirectSubordinates = new List<Participant>()
                    {
                        new Participant()
                        {
                            AmountOfSubordinates = 0,
                            DirectSubordinates = new List<Participant>(),
                            DirectSuperiorId = 1,
                            Id = 2,
                            Level = 1,
                            Sum = 0
                        },
                        new Participant()
                        {
                            AmountOfSubordinates = 1,
                            DirectSubordinates = new List<Participant>()
                            {
                                new Participant()
                                {
                                    AmountOfSubordinates = 0,
                                    DirectSubordinates = new List<Participant>(),
                                    DirectSuperiorId = 3,
                                    Id = 4,
                                    Level = 2,
                                    Sum = 0
                                }
                            },
                            DirectSuperiorId = 1,
                            Id = 3,
                            Level = 1,
                            Sum = 0
                        }
                    },
                    DirectSuperiorId = 0,
                    Id = 1,
                    Level = 0,
                    Sum = 0
                }
            };

            uint idToFind = 3;

            var expectedParticipant = new Participant()
            {
                AmountOfSubordinates = 1,
                DirectSubordinates = new List<Participant>()
                {
                    new Participant()
                    {
                        AmountOfSubordinates = 0,
                        DirectSubordinates = new List<Participant>(),
                        DirectSuperiorId = 3,
                        Id = 4,
                        Level = 2,
                        Sum = 0
                    }
                },
                DirectSuperiorId = 1,
                Id = 3,
                Level = 1,
                Sum = 0
            };

            //Act
            var foundParticipant = CalculateService.FindParticipantById(pyramid[0], idToFind);

            //Assert
            Assert.AreEqual(expectedParticipant.Id, foundParticipant.Id);
            Assert.AreEqual(expectedParticipant.DirectSubordinates.Count, foundParticipant.DirectSubordinates.Count);
            Assert.AreEqual(expectedParticipant.AmountOfSubordinates, foundParticipant.AmountOfSubordinates);
            Assert.AreEqual(expectedParticipant.Level, foundParticipant.Level);
            Assert.AreEqual(expectedParticipant.DirectSuperiorId, foundParticipant.DirectSuperiorId);
        }


        [TestMethod]
        public void WhenAddSubordinatesCountsToEachParticipantCalledWithGivenPyramidThenShouldAssignAmountOfSubordinates()
        {
            //Arrange
            var pyramid = new List<Participant>()
            {
                new Participant()
                {
                    AmountOfSubordinates = 0,
                    DirectSubordinates = new List<Participant>()
                    {
                        new Participant()
                        {
                            AmountOfSubordinates = 0,
                            DirectSubordinates = new List<Participant>(),
                            DirectSuperiorId = 1,
                            Id = 2,
                            Level = 1,
                            Sum = 0
                        },
                        new Participant()
                        {
                            AmountOfSubordinates = 0,
                            DirectSubordinates = new List<Participant>()
                            {
                                new Participant()
                                {
                                    AmountOfSubordinates = 0,
                                    DirectSubordinates = new List<Participant>(),
                                    DirectSuperiorId = 3,
                                    Id = 4,
                                    Level = 2,
                                    Sum = 0
                                }
                            },
                            DirectSuperiorId = 1,
                            Id = 3,
                            Level = 1,
                            Sum = 0
                        }
                    },
                    DirectSuperiorId = 0,
                    Id = 1,
                    Level = 0,
                    Sum = 0
                }
            };

            //Act
            CalculateService.AddSubordinatesCountsToEachParticipant(pyramid[0]);

            //Assert
            Assert.AreEqual(2, pyramid.First().DirectSubordinates.Count);
            Assert.AreEqual(0, pyramid.First().DirectSubordinates.First(s => s.Id == 2).DirectSubordinates.Count);
            Assert.AreEqual(1, pyramid.First().DirectSubordinates.First(s => s.Id == 3).DirectSubordinates.Count);
            Assert.AreEqual(0, pyramid.First()
                    .DirectSubordinates.First(s => s.Id == 3)
                    .DirectSubordinates.First(s => s.Id == 4)
                    .DirectSubordinates.Count);
        }

        [TestMethod]
        public void WhenSetProvisionSequenceCalledWithGivenFounderThenShouldSetProvisionSequence()
        {
            //Arrange
            var pyramid = new List<Participant>()
            {
                new Participant()
                {
                    AmountOfSubordinates = 0,
                    DirectSubordinates = new List<Participant>()
                    {
                        new Participant()
                        {
                            AmountOfSubordinates = 0,
                            DirectSubordinates = new List<Participant>(),
                            DirectSuperiorId = 1,
                            Id = 2,
                            Level = 1,
                            Sum = 0
                        },
                        new Participant()
                        {
                            AmountOfSubordinates = 0,
                            DirectSubordinates = new List<Participant>()
                            {
                                new Participant()
                                {
                                    AmountOfSubordinates = 0,
                                    DirectSubordinates = new List<Participant>(),
                                    DirectSuperiorId = 3,
                                    Id = 4,
                                    Level = 2,
                                    Sum = 0
                                }
                            },
                            DirectSuperiorId = 1,
                            Id = 3,
                            Level = 1,
                            Sum = 0
                        }
                    },
                    DirectSuperiorId = 0,
                    Id = 1,
                    Level = 0,
                    Sum = 0
                }
            };

            var founder = pyramid[0];
            var provisionSequence = new List<uint>();
            uint superiorId = 3;
            var expectedProvisionSequence = new List<uint>()
            {
                3,
                1
            };

            //Act
            CalculateService.SetProvisionSequence(provisionSequence, superiorId, founder);

            //Assert
            Assert.AreEqual(expectedProvisionSequence.Count, provisionSequence.Count);
            Assert.AreEqual(expectedProvisionSequence.First(), provisionSequence.First());
            Assert.AreEqual(expectedProvisionSequence[1], provisionSequence[1]);

        }


        [TestMethod]
        public void WhenSetProvisionSequenceCalledWithTransferFromOwnerThenShouldSetEmptyProvisionSequence()
        {
            //Arrange
            var pyramid = new List<Participant>()
            {
                new Participant()
                {
                    AmountOfSubordinates = 0,
                    DirectSubordinates = new List<Participant>()
                    {
                        new Participant()
                        {
                            AmountOfSubordinates = 0,
                            DirectSubordinates = new List<Participant>(),
                            DirectSuperiorId = 1,
                            Id = 2,
                            Level = 1,
                            Sum = 0
                        },
                        new Participant()
                        {
                            AmountOfSubordinates = 0,
                            DirectSubordinates = new List<Participant>()
                            {
                                new Participant()
                                {
                                    AmountOfSubordinates = 0,
                                    DirectSubordinates = new List<Participant>(),
                                    DirectSuperiorId = 3,
                                    Id = 4,
                                    Level = 2,
                                    Sum = 0
                                }
                            },
                            DirectSuperiorId = 1,
                            Id = 3,
                            Level = 1,
                            Sum = 0
                        }
                    },
                    DirectSuperiorId = 0,
                    Id = 1,
                    Level = 0,
                    Sum = 0
                }
            };

            var founder = pyramid[0];
            var provisionSequence = new List<uint>();
            uint superiorId = 0;
            var expectedProvisionSequence = new List<uint>();

            //Act
            CalculateService.SetProvisionSequence(provisionSequence, superiorId, founder);

            //Assert
            Assert.AreEqual(expectedProvisionSequence.Count, provisionSequence.Count);

        }


        [TestMethod]
        public void WhenAssignProvisionsToParticipantsThenShouldAssignSumsToEachParticipant()
        {
            //Arrange
            var pyramid = new List<Participant>()
            {
                new Participant()
                {
                    AmountOfSubordinates = 0,
                    DirectSubordinates = new List<Participant>()
                    {
                        new Participant()
                        {
                            AmountOfSubordinates = 0,
                            DirectSubordinates = new List<Participant>(),
                            DirectSuperiorId = 1,
                            Id = 2,
                            Level = 1,
                            Sum = 0
                        },
                        new Participant()
                        {
                            AmountOfSubordinates = 0,
                            DirectSubordinates = new List<Participant>()
                            {
                                new Participant()
                                {
                                    AmountOfSubordinates = 0,
                                    DirectSubordinates = new List<Participant>(),
                                    DirectSuperiorId = 3,
                                    Id = 4,
                                    Level = 2,
                                    Sum = 0
                                }
                            },
                            DirectSuperiorId = 1,
                            Id = 3,
                            Level = 1,
                            Sum = 0
                        }
                    },
                    DirectSuperiorId = 0,
                    Id = 1,
                    Level = 0,
                    Sum = 0
                }
            };

            var founder = pyramid[0];
            var provisionSequence = new List<uint>()
            {
                1,
                3
            };
            var currentParticipant = pyramid[0]
                .DirectSubordinates.First(s => s.Id == 3)
                .DirectSubordinates.First(s => s.Id == 4);
            uint valueOfTransfer = 200;
            ulong expectedSumForId_1 = 100;
            ulong expectedSumForId_2 = 0;
            ulong expectedSumForId_3 = 100;
            ulong expectedSumForId_4 = 0;


            //Act
            CalculateService.AssignProvisionsToParticipants(provisionSequence, founder, currentParticipant,
                valueOfTransfer);

            //Assert
            Assert.AreEqual(0, provisionSequence.Count);
            Assert.AreEqual(expectedSumForId_1, pyramid[0].Sum);
            Assert.AreEqual(expectedSumForId_3, pyramid[0].DirectSubordinates.First(s => s.Id == 3).Sum);
            Assert.AreEqual(expectedSumForId_2, pyramid[0].DirectSubordinates.First(s => s.Id == 2).Sum);
            Assert.AreEqual(expectedSumForId_4, pyramid[0]
                .DirectSubordinates.First(s => s.Id == 3)
                .DirectSubordinates.First(s => s.Id == 4).Sum);

        }
    }
}
